FROM node:6.9.0

ENV PORT 80
ENV SECURE_PORT 443

EXPOSE 80
EXPOSE 443

RUN npm install -g grunt-cli yarn && npm install -g bower

ADD . /build

RUN cd /build \
    && yarn install \
    && bower install --allow-root \
    && grunt build \
    && mkdir /app \
    && mv dist /app \
    && mv node_modules /app \
    && rm -rf /build

ENV NODE_ENV production

WORKDIR /app

CMD ["node", "dist/server/app.js"]
