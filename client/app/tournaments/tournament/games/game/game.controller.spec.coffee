'use strict'

describe 'Controller: GameCtrl', ->

  # load the controller's module
  beforeEach module 'shpeelyApp'
  GameCtrl = undefined
  scope = undefined

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
