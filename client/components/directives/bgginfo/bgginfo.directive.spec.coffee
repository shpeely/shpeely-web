'use strict'

describe 'Directive: bgginfo', ->

  # load the directive's module and view
  beforeEach module 'shpeelyApp'
  beforeEach module 'components/bgginfo/bgginfo.html'
  element = undefined
  scope = undefined
  beforeEach inject ($rootScope) ->
    scope = $rootScope.$new()
