'use strict'

describe 'Directive: message', ->

  # load the directive's module and view
  beforeEach module 'shpeelyApp'
  beforeEach module 'components/directives/message/message.html'
  element = undefined
  scope = undefined
  beforeEach inject ($rootScope) ->
    scope = $rootScope.$new()
