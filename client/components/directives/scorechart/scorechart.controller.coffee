
angular.module 'shpeelyApp'
.controller 'ScoreChartCtrl', ($scope, $timeout, $window) ->

  $scope.chartLoading = true

  $scope.scoreChartStyle =
    width: $window.innerWidth - 50 + 'px'
    display: 'none'

  $scope.changeTimespan = (fromTime, toTime)=>
    @fromTime = fromTime
    @toTime = toTime
    createChart $scope.timeSeries

  createChart = (timeSeries)=>
    if !(timeSeries and timeSeries.series.length and @fromTime) then return

    # get date of the first result
    $scope.startDate = _.chain $scope.timeSeries.meta
      .take()
      .map 'time'
      .map ((t) -> new Date(t))
      .first()
      .value()

    firstIndex = _.findIndex timeSeries.meta, (result, i)=> result.time > @fromTime.getTime()
    if firstIndex < 0 then firstIndex = timeSeries.series[0].data.length

    if @toTime
      [..., afterToTime] = _.partition timeSeries.meta, (x)=> x.time <= @toTime.getTime()
      tailIndex = afterToTime.length
    else
      tailIndex = 0

    reverseIndex = timeSeries.meta.length - firstIndex

    data = _.chain timeSeries.series
      .map (serie)->
        subtract = 0
        if serie.data.length > reverseIndex
          subtract = serie.data[(serie.data.length - reverseIndex) - 1].y

        score = _.chain serie.data
          .dropRight tailIndex
          .last()
          .value()?.y

        score: if score then score - subtract else null
        player:
          name: serie.name

    data = _.chain(data)
      .filter (e)-> e.score
      .sortBy (e)-> -e.score
      .value()

    players = _.map data, 'player.name'
    data = _.chain data
      .map 'score'
      .map (score) -> {color: (if score < 0 then '#E74C3C' else '#18BC9C'), y: score}
      .value()

    $scope.scorechartConfig =
      options:
        chart:
          type: 'column'
        exporting:
          enabled: false
        #credits:
          #enabled: false
      # higharts-ng options
      title:
        text: ''
      series: [ {
        name: 'Score'
        showInLegend: false
        data: data
      } ]
      xAxis:
        categories: players
      yAxis:
        title:
          text: 'Score'
      func: (c)->
        $scope.chartLoading = false
        $scope.scoreChartStyle = {}
        $timeout (-> c.reflow()), 0

  $scope.$watch 'timeSeries', createChart
  createChart($scope.timeSeries)
