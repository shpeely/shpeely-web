'use strict'

describe 'Directive: scorechart', ->

  # load the directive's module and view
  beforeEach module 'shpeelyApp'
  beforeEach module 'components/directives/scorechart/scorechart.html'
  element = undefined
  scope = undefined
  beforeEach inject ($rootScope) ->
    scope = $rootScope.$new()
