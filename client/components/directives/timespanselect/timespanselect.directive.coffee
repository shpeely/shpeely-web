'use strict'

angular.module 'shpeelyApp'
.directive 'timespanselect', ->
  templateUrl: 'components/directives/timespanselect/timespanselect.html'
  restrict: 'E'
  scope:
    onChange: '&'
    startDate: '='
  link: (scope) ->

    scope.selectTimespan = (timespan)->
      scope.selectedTimespan = timespan.name
      scope.onChange
        fromTime: timespan.from
        toTime: timespan.to

    getTimespanOptions = (startDate)->
      now = moment()

      currentDate = moment(startDate)
      if startDate
        pastYears = while currentDate.isBefore(now)
          item =
            name: "Year #{currentDate.startOf('year').format('YYYY')}"
            from: currentDate.startOf('year').toDate()
            to: currentDate.clone().add(1, 'year').startOf('year').toDate()
          currentDate = currentDate.clone().add 1, 'year'
          item
        pastYears[pastYears.length - 1].default = true
      else
        pastYears = []

      return [
        pastYears...
        { name: "#{now.clone().format('MMMM YYYY')}",   from: moment().startOf('month').toDate() }
        { name: "Past Year",   from: now.clone().subtract(1, 'year').toDate() }
        { name: "Past 6 Month",   from: now.clone().subtract(6, 'month').toDate() }
        { name: "Past 30 Days",   from: now.clone().subtract(30, 'days').toDate() }
        { name: "Past 7 Days",   from: now.clone().subtract(7, 'days').toDate() }
        { name: "All Time",   from: new Date(0), bold: true, default: !scope.startDate}
      ]

    scope.$watch 'startDate', (newStartDate) ->
      scope.timespanOptions = getTimespanOptions(newStartDate)
      scope.selectTimespan(_.find scope.timespanOptions, 'default')
    , true
