'use strict'

$ = angular.element

describe 'Directive: timespanselect', ->

  # load the directive's module and view
  beforeEach module 'shpeelyApp'
  beforeEach module 'components/directives/timespanselect/timespanselect.html'
  element = undefined
  scope = undefined
  compile = undefined

  beforeEach inject ($rootScope, $compile) ->
    scope = $rootScope.$new()
    compile = $compile
    element = angular.element '<timespanselect on-change="onChange" start-date="startDate"></timespanselect>'

  it 'should create the correct time spans', ->
    # given
    scope.startDate = moment('2012-01-01').toDate()
    console.log(scope.startDate)
    element = compile(element) scope
    scope.$apply()

    items = element.find('li')
      .map (x, el)-> el.innerText
      .toArray()

    expect(items[...3]).toEqual(['Year 2012', 'Year 2013', 'Year 2014'])
