'use strict'

describe 'Directive: tournament', ->

  # load the directive's module and view
  beforeEach module 'shpeelyApp'
  beforeEach module 'components/directives/tournament/tournament.html'
  element = undefined
  scope = undefined
  beforeEach inject ($rootScope) ->
    scope = $rootScope.$new()
