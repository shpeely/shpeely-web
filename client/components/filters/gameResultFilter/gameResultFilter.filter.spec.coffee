'use strict'

describe 'Filter: gameResultFilter', ->

  # load the filter's module
  beforeEach module 'shpeelyApp'

  # initialize a new instance of the filter before each test
  gameResultFilter = undefined
  beforeEach inject ($filter) ->
    gameResultFilter = $filter 'gameResultFilter'
