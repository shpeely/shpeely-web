'use strict'

describe 'Service: BoardgameGeek', ->

  # load the service's module
  beforeEach module 'shpeelyApp'

  # instantiate service
  BoardgameGeek = undefined
  beforeEach inject (_BggApi_) ->
    BoardgameGeek = _BggApi_

  it 'should do something', ->
    expect(!!BoardgameGeek).toBe true
