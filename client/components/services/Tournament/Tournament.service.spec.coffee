'use strict'

describe 'Service: ActiveTournament', ->

  # load the service's module
  beforeEach module 'shpeelyApp'

  # instantiate service
  ActiveTournament = undefined
  beforeEach inject (_ActiveTournament_) ->
    ActiveTournament = _ActiveTournament_
