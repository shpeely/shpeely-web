var _ = require('lodash');
var fetch = require('node-fetch');

var os = require('os');
var config = require('../../config/environment');
var BggDataModel = require('./bggdata.model.js');

// Cache for responses from BGG
var TTL = 3600 * 24 * 3; // cache for 7 days
var CachemanMongo = require('cacheman-mongo');
var cache = new CachemanMongo(config.mongo.uri, {
  collection: 'bggdatacache'
});

// BGG API
var bgg = require('bgg')({
  timeout: 10e3,
  retry: {
    initial: 100,
    multiplier: 1.5,
    max: 750,
  }
});

// Simple wrapper for BGG api
var BGGData = function () {};

BGGData.prototype.cache = cache;
const memorycache = {};
const promises = {};

BGGData.prototype.search = function (query, cb) {

  const cacheKey = 'search:' + query;

  // check cache first
  cache.get(cacheKey, function(err, val) {
    if(err) return cb(err);

    if(val) {
      return cb(null, val);
    } else {
      fetch(`${config.shpeelyApiUrl}/bgg/search?query=${query}`)
        .then(res => res.json())
        .then((items) => {
          cache.set(cacheKey, items, TTL, cb);
        })
        .catch(cb)
    }
  });

};

var getCacheKey = function(bggid) {
  return 'info:' + bggid;
};

BGGData.prototype.info = function (bggid) {

  if(memorycache[bggid]) {
    // found in cache
    if (memorycache[bggid].then) {
      // return unresolved promise
      return memorycache[bggid];
    }
    // return resolved promimse
    return Promise.resolve(memorycache[bggid])
  }

  let promise = fetch(`${config.shpeelyApiUrl}/bgg/${bggid}`)
    .then(res => (res.ok ? res : fetch(`${config.shpeelyApiUrl}/bgg/sync/${bggid}`, { method: 'PUT' })))
    .then(res => res.json())
    .then(json => { memorycache[bggid] = json; return json; })
    .catch((err) => {
      delete memorycache[bggid];
      console.error('failed to get ', bggid);
    });

  memorycache[bggid] = promise;

  return promise;
};

BGGData.prototype.shortInfo = function (bggid, cb) {
  this.info(bggid)
    .then(data => ({id: data.bggid, name: data.name }))
    .then(shortInfo => cb(null, shortInfo))
    .catch(cb);
};

module.exports = new BGGData();
